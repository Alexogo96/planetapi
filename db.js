/*
    update planet object skeleton
    {
        "planet":{
            "id":1 (some number)
        }
    }

*/

const { Pool } = require('pg');//binding database

class DB{

    constructor(){
        this.config = {
            user:'postgres',
            host: 'localhost',
            database: 'planets',
            password: '4l3X2307',
            port: 5432
        };
        this.pool = new Pool(this.config);

    }
    getPlanets(req,res){
        this.pool.query('SELECT * FROM planet', (err, results)=>{
            if(err){
                res.status(500);
                res.json({
                    success: false,
                    message: err
                });
            }else{
                res.status(200);
                res.json(results.rows);
            }
            
        });
    

    }
    addPlanet(req,res){
        var { planet } = req.body;
        //Checking for existance of planet by name
        this.pool.query('SELECT * FROM planet WHERE name=$1', [planet.name], (err, results)=>{
            if(results.rowCount!==0){
                res.status(500);
                res.json({
                    success: false,
                    message: "Planet already exists"
                });
            }else{
                //Inserting planet if the name is not already in database
                this.pool.query( 'INSERT INTO planet (name, size, date_discovered) VALUES($1, $2, $3)', [planet.name, planet.size, planet.date_discovered], (err, results)=>{
                    if(err){
                        res.status(500);
                        res.json({
                            success: false,
                            message: err
                        });
                    }else{
                        res.status(200);
                        res.json({
                            success:true,
                            message: 'planet added'
                        });
                    }
                });
            }
        });
        
        
    }
    updatePlanet(req,res){
        var { planet } = req.body;
        //Checking for existance of planet by name
        this.pool.query('SELECT * FROM planet WHERE id=$1', [planet.id], (err, results)=>{
            if(results.rowCount!==0){
                let newPlanet = {
                    id:planet.id,
                    name: planet.name || results.rows[0].name,
                    size: planet.size || results.rows[0].size,
                    date_discovered: planet.date_discovered || results.rows[0].date_discovered
                }//getting property from results if they don't exist in the body

                this.pool.query('UPDATE planet SET name=$1, size=$2, date_discovered=$3 WHERE id=$4', [newPlanet.name, newPlanet.size, newPlanet.date_discovered, planet.id], (err, results)=>{
                    if(err){
                        res.status(500);
                        res.json({
                            success: false,
                            message: err
                        });
                    }else{
                        res.status(200);
                        res.json({
                            success:true,
                            message: 'planet updated'
                        });
                    }
                });
            }else{
                res.status(500);
                res.json({
                    success: false,
                    message: "Planet not in database exists"
                });
                //Planetname doesn't exist
                
            }
        });
        
        
    }
    deletePlanet(req,res){
        var { planet } = req.body;
        this.pool.query('DELETE FROM planets WHERE name=$1', [planet.name],(err, results)=>{
            if(err){
                res.status(500);
                res.json({
                    success: false,
                    message: err
                });
            }else if(results.rowCount===0){
                res.status(500);
                res.json({
                    success: false,
                    message: "Nothing was deleted"
                });
            }else{
                res.status(200);
                res.json({
                    success:true,
                    message: `planet with name ${planet.name} has been deleted`
                });
            }
        });
    }
    getPool(){
        return this.pool;
    }
}

module.exports.db = new DB();