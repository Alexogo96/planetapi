const express = require('express');
const { db } = require('./db');
const {auth} = require('./auth')
const port = 8080;
const app = express();
const publicPath = ['/api/v1/auth/generate-key'];

const Auth = new auth(db.getPool());

app.use(express.json());
app.use(express.urlencoded({
    extended:false
}));
app.use( async (req, res, next)=>{
    if(publicPath.includes(req.originalUrl)) return next();
    const authResult = await Auth.authenticateKey(req, res);
    if(authResult===true){
        next();
    }else{
        res.status(401).json(authResult);
    }
});

app.get('/api/v1/planets', (req,res)=>{
    db.getPlanets(req,res);
});
app.post('/api/v1/planets/add',(req,res)=>{
    db.addPlanet(req,res);
});
app.delete('/api/v1/planets/delete', (req,res)=>{
    db.deletePlanet(req,res);
});
app.patch('/api/v1/planets/update', (req,res)=>{
    db.updatePlanet(req,res);
})
app.post('/api/v1/auth/generate-key', (req,res) => Auth.generateKey(req,res));

app.listen(port, ()=>{
    console.log(`Server has connected to port ${port}`)
});