const crypto = require('crypto');


class auth{
    constructor(pool){
        this.pool = pool;
    }
    async generateKey(req,res) {
        try{
            const apiKey = await crypto.randomBytes(32).toString('hex');
            const result = await this.pool.query('INSERT INTO api_key (key) VALUES($1) RETURNING ID', [apiKey]);
            //check if result isn't empty
            
            res.json({
                apiKey
            });
        }catch(e){
            res.json({
                error: e.message
            });
        }
    }
    async authenticateKey(req, res){
        try{
            const { authorization } = req.headers;
            if(!authorization){
                return{
                    error: 'No API key found.'
                };
            }
            const key = authorization.split(' ')[1];
            const keyResult = await this.pool.query('SELECT * FROM api_key WHERE key = $1 AND active = true',[key]);
            if (keyResult.rowCount>0){
                return true;
            }else{
                return{
                    error: 'Invalid api_key'
                }
            }
        }catch(e){
            return{
                erorr: e.message
            }
        }
    }
}


module.exports.auth = auth;